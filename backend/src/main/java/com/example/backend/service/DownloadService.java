package com.example.backend.service;

import jakarta.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Service
public class DownloadService {

    public void downloadZip(HttpServletResponse response) {
        response.setContentType("application/zip");
        response.setHeader("Content-Disposition", "attachment; filename=download.zip");

        try(ZipOutputStream zout = new ZipOutputStream(response.getOutputStream())) {
            ZipEntry entry1 = new ZipEntry("1.json");
            zout.putNextEntry(entry1);
            zout.write("содержание 1".getBytes());
            zout.closeEntry();
            zout.finish();
        } catch (IOException e) {
//            logger.error(e.getMessage(), e);
        }
    }
}
