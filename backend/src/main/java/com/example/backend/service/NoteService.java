package com.example.backend.service;


import com.example.backend.entity.Note;
import com.example.backend.exception.NoSuchElementFoundException;
import com.example.backend.exception.NotFoundNote;
import com.example.backend.repo.NoteRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
@AllArgsConstructor
public class NoteService {
    private final NoteRepository repository;
    // GET ONE
    public Note getNoteById(String username, Long id) throws NoSuchElementFoundException {
        Note note = repository.findByUsernameAndId(username, id);

        if (note == null) {
            throw new NoSuchElementFoundException("dawdawdadwaw");
        }

        return note;
    }

    // GET ALL
    public List<Note> getNotesByUsername(String username) {
        return repository.findAllByUsernameOrderByCreatedDesc(username);
    }

    // CREATE
    public Note addNote(Note note){
        return repository.save(note);
    }

    // UPDATE
    public Note update(Note note){
        return repository.save(note);
    }

    // DELETE
    public void delete(Long id){
        repository.deleteById(id);
    }
}
