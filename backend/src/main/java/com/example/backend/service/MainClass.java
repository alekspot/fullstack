package com.example.backend.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class MainClass {
    public static void main(String[] args) throws IOException {
//        createFile();
        createZip();
    }

    private static void createFile() throws IOException {
        String fileName = "C:\\tmp\\download\\test7.json";
        String fileContent = "текст файла";

        Path fileToCreate = Paths.get(fileName);

        if (Files.exists(fileToCreate)) {
            System.out.println("Файл уже существует");
        } else {
            Path createdFile = Files.createFile(fileToCreate);

            try (FileOutputStream outputStream = new FileOutputStream(createdFile.toFile())) {
                outputStream.write(fileContent.getBytes());

                System.out.println("Файл " + createdFile + " создан");
                System.out.println("Содержимое файла: ");
                System.out.println(fileContent);

            } catch (IOException e) {
                System.out.println("Ошибка");
            }
        }
    }

    private static void createZip() {
        String zipname = "C:\\tmp\\download\\test3.zip";

        // Создать zip file
        try (
                ZipOutputStream zout = new ZipOutputStream(new FileOutputStream(zipname));
        ) {
            // Запись 1го файла
            ZipEntry entry1 = new ZipEntry("1.json");
            zout.putNextEntry(entry1);
            zout.write("содержание 1".getBytes());
            zout.closeEntry();

            // Запись 2го файла
            ZipEntry entry2 = new ZipEntry("2.json");
            zout.putNextEntry(entry2);
            zout.write("содержание 2".getBytes());
            zout.closeEntry();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }
}
