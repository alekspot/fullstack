package com.example.backend.controller;

import com.example.backend.service.DownloadService;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@RestController
@RequestMapping("/api/backup")
public class BackupController {
    @Autowired
    private DownloadService downloadService;

    @PostMapping(value = "/export", produces = "application/zip")
    public ResponseEntity<byte[]> zipFilesFromServer() throws Exception{

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(byteArrayOutputStream);
        ZipOutputStream zipOutputStream = new ZipOutputStream(bufferedOutputStream);
        ZipEntry entry1 = new ZipEntry("1.json");
        zipOutputStream.putNextEntry(entry1);
        zipOutputStream.write("содержание 1".getBytes());
        zipOutputStream.closeEntry();
        zipOutputStream.finish();

        zipOutputStream.close();


        IOUtils.closeQuietly(bufferedOutputStream);
        IOUtils.closeQuietly(byteArrayOutputStream);


        return ResponseEntity
                .ok()
                .header("Content-Disposition", "attachment; filename= Result_files.zip")
                .body(byteArrayOutputStream.toByteArray());
    }
}
