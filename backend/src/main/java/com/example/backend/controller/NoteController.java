package com.example.backend.controller;

import com.example.backend.entity.Note;
import com.example.backend.exception.NoSuchElementFoundException;
import com.example.backend.exception.NotFoundNote;
import com.example.backend.exception.NotFoundNoteError;
import com.example.backend.service.NoteService;
import lombok.AllArgsConstructor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@AllArgsConstructor
@RequestMapping("/api/notes")
public class NoteController {
    private NoteService service;

    @GetMapping("/{username}/all")
    public ResponseEntity<List<Note>> getUserNotes(@PathVariable("username") String username) {
        return  ResponseEntity.ok(service.getNotesByUsername(username));
    }

    @GetMapping("/{username}/{id}")
    public ResponseEntity<Note> getNote(@PathVariable("username") String username, @PathVariable("id") Long id) {
        Note note= service.getNoteById(username, id);

        return ResponseEntity.ok(note);
    }

    @PostMapping("/add")
    public ResponseEntity<Note> addNote(@RequestBody Note note, @AuthenticationPrincipal Jwt jwt) {
        String username = jwt.getClaimAsString("preferred_username");
        note.setUsername(username);
        note.setCreated(new Date());
        note.setPinned(false);
        return  ResponseEntity.ok(service.addNote(note));
    }

    @PutMapping("/update/{username}/{id}")
    public ResponseEntity<Note> update(@RequestBody Note note, @AuthenticationPrincipal Jwt jwt, @PathVariable("username") String usernameFromPath, @PathVariable("id") Long id) {
        System.out.println("PUT" + "/{username}/{id}");
        String username = jwt.getClaimAsString("preferred_username");

        System.out.println();

        if(usernameFromPath.equals(username)) {
            note.setUsername(username);
            return ResponseEntity.ok(service.update(note));
        }

        return ResponseEntity.badRequest().build();
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<Note> delete(@PathVariable("id") Long id) {

        try {
            service.delete(id);
        } catch (EmptyResultDataAccessException e) {
            return new ResponseEntity("Id" + id + "not found", HttpStatus.NOT_ACCEPTABLE);
        }

        return new ResponseEntity(id, HttpStatus.OK);
    }

    @ExceptionHandler(NoSuchElementFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseEntity handleNoSuchElementFoundException(
            NoSuchElementFoundException exception
    ) {

        NotFoundNoteError error = new NotFoundNoteError("Нет записи");
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(error);
    }


}
