package com.example.backend.controller;

import com.example.backend.model.Response;
import com.example.backend.model.UserDTO;
import com.example.backend.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/users") // базовый URI
public class UserController {
    private UserService userService;
    @GetMapping("/all")
    public ResponseEntity<List<UserDTO>> getUsers() {

        return ResponseEntity.ok(userService.getAllUsers());
    }

}
