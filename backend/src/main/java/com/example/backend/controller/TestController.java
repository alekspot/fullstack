package com.example.backend.controller;

import com.example.backend.entity.Note;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class TestController {
    @GetMapping("/test")
    ResponseEntity<Note> getTest() {
        Note note = new Note();
        note.setUsername("Test");
        note.setTitle("Тестовый заголовок");
        return ResponseEntity.ok(note);
    }
}
