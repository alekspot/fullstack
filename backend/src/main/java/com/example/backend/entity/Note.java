package com.example.backend.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Entity
@NoArgsConstructor
@Getter
@Setter
@Table(name = "item", schema = "app", catalog = "postgres")
public class Note {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    @JsonIgnore
    @Column(name = "category_id")
    private Long categoryId;

    private String description;
    @JsonIgnore
    private String username;

    private Date created;

    private boolean pinned;

    private String icon;
}
