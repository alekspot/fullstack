package com.example.backend.exception;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CustomAccessDeniedHandler implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException exc) throws IOException {
        final Map<String, Object> jsonBody = new HashMap<>() ;

        jsonBody.put("da", "dawwdaw") ;
        jsonBody.put("adw", "da") ;
        response.setContentType("application/json") ;
        response.setStatus(HttpServletResponse.SC_FORBIDDEN) ;

        final ObjectMapper mapper = new ObjectMapper() ;
        mapper.writeValue(response.getOutputStream(), jsonBody) ;
    }
}
