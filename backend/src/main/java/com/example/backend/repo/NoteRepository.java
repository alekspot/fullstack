package com.example.backend.repo;

import com.example.backend.entity.Note;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NoteRepository extends JpaRepository<Note, Long> {
    List<Note> findAllByUsernameOrderByCreatedDesc(String username);

    Note findByUsernameAndId(String username, Long id);
}
