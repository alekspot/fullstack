package com.example.bff.controller;

import com.example.bff.feign.AuthFeignClient;
import com.example.bff.feign.BackupFeignClient;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/backup")
@AllArgsConstructor
public class BackupController {
    private BackupFeignClient backupClient;

@PostMapping("/export")
    public ResponseEntity<byte[]> export(){
        return backupClient.export();
    }
}
