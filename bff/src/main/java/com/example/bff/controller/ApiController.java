package com.example.bff.controller;


import com.example.bff.exception.CustomServerErrorException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.*;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.io.IOException;

//@RestController
public class ApiController {
    @Value("${resourceserver.url}")
    private String resourceServerURL;
    WebClient client = WebClient.create();

    @RequestMapping("/api/**")
    public Mono <ResponseEntity<Object>> data(
            @RequestBody(required = false) String body,
            HttpMethod method,
            HttpServletRequest request,
            HttpServletResponse response,
            @CookieValue(value = "AT", required = false) String accessToken
    ) {

        WebClient.Builder wb = WebClient.builder()
                .baseUrl(resourceServerURL + request.getRequestURI())

                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        if (accessToken != null) {
            wb.defaultHeader(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken);
        }


        WebClient client = wb.build();


        return client.method(method)
                .body(body == null ? null : BodyInserters.fromValue(body))
                .exchangeToMono(clientResponse -> clientResponse.toEntity(Object.class));
    }
}
