package com.example.bff.controller;

import com.example.bff.dto.User;
import com.example.bff.feign.BackupFeignClient;
import com.example.bff.feign.AuthFeignClient;
import com.example.bff.model.Token;
import com.example.bff.service.AuthService;
import com.fasterxml.jackson.core.JsonProcessingException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;


@RestController
@RequestMapping("/")
public class AuthController {
    @Value("${keycloak.clientid}")
    private String clientId;

    @Value("${keycloak.granttype.code}")
    private String grantTypeCode;

    @Value("${keycloak.granttype.refresh}")
    private String grantTypeRefresh;

    @Value("${client.url}")
    private String clientURL;

    @Value("${keycloak.secret}")
    private String clientSecret;

    private AuthFeignClient authClient;

    private AuthService authService;

    private BackupFeignClient apiCLient;

    private JSONObject payload;

    @Autowired
    public AuthController(AuthFeignClient authClient, AuthService authService, BackupFeignClient apiCLient) {
        this.authClient = authClient;
        this.authService = authService;
        this.apiCLient = apiCLient;
    }

    @Value("${resourceserver.url}")
    private String resourceServerURL;
    WebClient client = WebClient.create();

    @RequestMapping(value = "/api/**", produces = "*/*")
    public ResponseEntity<?> data(
            @RequestBody(required = false) String body,
            HttpMethod method,
            HttpServletRequest request,
            HttpServletResponse response,
            @CookieValue(value = "AT", required = false) String accessToken,
            @CookieValue(value = "RT", required = false) String refreshToken
    ) {

        System.out.println("BASE_URL=" + resourceServerURL + request.getRequestURI());
        System.out.println("getRequestURI=" + request.getRequestURI());


//        ResponseEntity<?> res = null;
//        try {
//            res= apiCLient.requestGet(request.getRequestURI());
//        } catch (FeignException e) {
//            System.out.println(e);
//
//            Map<String, Collection<String>> mapForm = e.responseHeaders();
//            String responseBody = StandardCharsets.UTF_8.decode(e.responseBody().get()).toString();
//
//
//            return ResponseEntity.ok(responseBody);
//        }
//        return res;

        WebClient.Builder wb = WebClient.builder()
                .baseUrl(resourceServerURL + request.getRequestURI())
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE);

        if (accessToken != null) {
            wb.defaultHeader(HttpHeaders.AUTHORIZATION, "Bearer " + accessToken);
        }

        HttpHeaders tokenHeaders = null;

        if (accessToken == null && refreshToken != null) {
            try {
                Token token = updateToken(refreshToken);
                wb.defaultHeader(HttpHeaders.AUTHORIZATION, "Bearer " + token.getAccessToken());
                tokenHeaders = authService.createTokenCookies(token);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }


        WebClient client = wb.build();

        ResponseEntity<Object> res = client.method(method).accept(MediaType.ALL)
                .body(body == null ? null : BodyInserters.fromValue(body))
                .exchangeToMono(clientResponse -> clientResponse.toEntity(Object.class))
                .block();

        HttpHeaders headers = HttpHeaders.writableHttpHeaders(res.getHeaders());

        if (tokenHeaders != null) {
            headers.addAll(tokenHeaders);
        }

        return new ResponseEntity<>(res.getBody(), headers, res.getStatusCode());
    }

    @PostMapping("/auth/token")
    public ResponseEntity<String> token(@RequestBody String code) {
        // параметры запроса
        MultiValueMap<String, String> mapForm = new LinkedMultiValueMap<>();
        mapForm.add("grant_type", grantTypeCode);
        mapForm.add("client_id", clientId);
        mapForm.add("client_secret", clientSecret);
        mapForm.add("code", code);
        mapForm.add("redirect_uri", clientURL);

        ResponseEntity<String> response = authClient.getTokenByAuthCode(mapForm);

        try {
            // считать данные из JSON и записать в куки
            Token token = authService.getTokenFromResponse(response);
            HttpHeaders headersWithSecureCookies = authService.createTokenCookies(token);

            return ResponseEntity.ok().headers(headersWithSecureCookies).build();


        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/auth/exchange")
    public ResponseEntity<String> exchange(@CookieValue("RT") String oldRefreshToken) {
        Token token = updateToken(oldRefreshToken);

        if (token == null) {
            return ResponseEntity.badRequest().build();
        }

        try {
            HttpHeaders headersWithSecureCookies = authService.createTokenCookies(token);

            return ResponseEntity.ok().headers(headersWithSecureCookies).build();

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return ResponseEntity.badRequest().build();
    }

    @GetMapping("/auth/logout")
    public ResponseEntity<String> logout(@CookieValue("RT") String refreshToken) {

        MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
        requestBody.add("client_id", clientId);
        requestBody.add("client_secret", clientSecret);
        requestBody.add("refresh_token", refreshToken);

        authClient.logout(requestBody);

        // отправляем клиенту ответ с куками, которые автоматически применятся к браузеру
        return ResponseEntity.ok().headers(authService.clearCookies()).build();
    }


    @GetMapping("/auth/profile")
    public ResponseEntity<User> getProfile(@CookieValue("IT") String idToken) {
        return ResponseEntity.ok(authService.getProfile(idToken));
    }

    private Token updateToken(String oldRefreshToken) {
        System.out.println("Обновление токенов");

        // параметры запроса
        MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
        requestBody.add("grant_type", grantTypeRefresh);
        requestBody.add("client_id", clientId);
        requestBody.add("client_secret", clientSecret);
        requestBody.add("refresh_token", oldRefreshToken);

        ResponseEntity<String> response = authClient.updateToken(requestBody);

        try {
            return authService.getTokenFromResponse(response);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return null;
    }
}
