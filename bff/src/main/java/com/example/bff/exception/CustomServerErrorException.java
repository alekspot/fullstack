package com.example.bff.exception;

public class CustomServerErrorException extends Exception {
    public CustomServerErrorException(String message) {
        super(message);
    }
}