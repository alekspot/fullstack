package com.example.bff.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "backend", url = "${resourceserver.url}/api/backup")
public interface BackupFeignClient {

   @PostMapping("/export")
    ResponseEntity<byte[]> export();
}
