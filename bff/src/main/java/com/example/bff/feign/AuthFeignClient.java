package com.example.bff.feign;

import feign.Body;
import feign.Headers;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(name = "auth", url = "${keycloak.url}")
public interface AuthFeignClient {

    @PostMapping("/token")
    @Headers("Content-Type: application/x-www-form-urlencoded")
    ResponseEntity<String> getTokenByAuthCode(@RequestBody MultiValueMap<String, String> body);

    @PostMapping("/token")
    @Headers("Content-Type: application/x-www-form-urlencoded")
    ResponseEntity<String> updateToken(@RequestBody MultiValueMap<String, String> body);

    @PostMapping("/logout")
    @Headers("Content-Type: application/x-www-form-urlencoded")
    ResponseEntity<String> logout(@RequestBody MultiValueMap<String, String> body);

}
