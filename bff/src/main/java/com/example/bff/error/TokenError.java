package com.example.bff.error;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TokenError {
    private int status = 401;
    private String msg = "Токены протухли, необходимо авторизоваться";
}
