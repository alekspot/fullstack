package com.example.bff.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class Token {
    private String accessToken;
    private String idToken;
    private String refreshToken;
    private int accessTokenDuration;
    private int refreshTokenDuration;
}
