package com.example.bff.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter

// полные данные профиля пользователя, для отображения в frontend
public class User {

    private String id;
    private String username;


    // можно добавлять любые поля, которые вам необходимы (из keycloak или другого Auth Server)

}
