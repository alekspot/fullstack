ALTER TABLE IF EXISTS app.item
    ADD COLUMN pinned boolean default false NOT NULL;

ALTER TABLE IF EXISTS app.item
    ADD COLUMN created timestamp without time zone NOT NULL DEFAULT (now())::timestamp without time zone;