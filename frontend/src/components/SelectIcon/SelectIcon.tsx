import { useRef, useState } from 'react';
import style from './SelectIcon.module.scss';
import { IoLogoJavascript } from 'react-icons/io';
import { FaDocker, FaJava, FaRegStar, FaRegHeart } from 'react-icons/fa';
import { IoCloseOutline } from 'react-icons/io5';
import { BsFiletypeCss } from 'react-icons/bs';
import { useOutsideClick } from '@/hooks/useOutsideClick';

type Props = {
  handleSelect: (name: string) => void;
  onClose: () => void;
};

export const IconMap = {
  IoLogoJavascript,
  FaDocker,
  FaJava,
  BsFiletypeCss,
  FaRegStar,
  FaRegHeart,
};

const icons = Object.entries(IconMap);

export const SelectIcon = ({ onClose, handleSelect }: Props) => {
  const ref = useRef<HTMLDivElement | null>(null);
  useOutsideClick(ref, onClose);

  return (
    <div className={style.wrap} ref={ref}>
      <button className={style.button} onClick={onClose}>
        <IoCloseOutline size={20} />
      </button>
      <h3 className={style.title}>Выберите иконку</h3>
      <div className={style.icons}>
        {icons.map(([name, Icon], index) => (
          <div
            onClick={() => {
              handleSelect(name);
              onClose();
            }}
            className={style.icon}
            key={index}
          >
            <Icon size={30} />
          </div>
        ))}
      </div>
    </div>
  );
};
