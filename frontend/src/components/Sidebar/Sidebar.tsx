import { FC, ReactNode } from 'react';
import styles from './Sidebar.module.scss';
import { PortalWithTransition } from '../Portal/PortalWithTransition';

type Props = {
  isOpen?: boolean;
  handleClose?: () => void;
  position?: 'left' | 'right';
  children?: ReactNode;
};

export const Sidebar: FC<Props> = ({
  handleClose,
  isOpen = true,
  children,
  position = 'left',
}: Props) => {
  return (
    <PortalWithTransition
      isOpen={isOpen}
      classNames={{
        wrap: styles[`${position}-wrap`],
        start: styles[`${position}-start`],
        active: styles[`${position}-active`],
        end: styles[`${position}-end`],
      }}
    >
      <div className={styles.sidebar}>
        {handleClose && (
          <button className={styles.close} onClick={() => handleClose()}>
            x
          </button>
        )}
        {children}
      </div>
    </PortalWithTransition>
  );
};
