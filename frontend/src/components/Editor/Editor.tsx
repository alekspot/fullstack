'use client';

import { FC, useEffect, useState } from 'react';
import style from './Editor.module.scss';
import '@uiw/react-md-editor/markdown-editor.css';
import '@uiw/react-markdown-preview/markdown.css';
import MDEditor from '@uiw/react-md-editor';
import { Note } from '@/types/Note';
import { useQuery } from '@tanstack/react-query';
import { getNote } from '@/api/notes';
import { useParams } from 'next/navigation';
import { useIsUserPage, useProfile } from '@/providers/AuthProvider';
import { useNoteMenu } from '@/hooks/useNoteMenu';
import Link from 'next/link';
import { Button } from '../Button/Button';
import { Modal } from '../Modal/Modal';
import { IconMap, SelectIcon } from '../SelectIcon/SelectIcon';

type Props = {
  note?: Note;
};

const template =
  '# Новая заметка\n\n### Пример кода:\n\n```javascript\nconsole.log(\'Hello\');\n```\n\n```java\nSystem.out.println("Hello");\n```\n\n```bash\necho Hello\n```\n\n### Вставка картинки:\n\n![image](https://chimerical-madeleine-a2301a.netlify.app/img/skills/react.jpg)';
export const Editor: FC<Props> = ({ note }) => {
  const { username: usernameFromURL, noteId } = useParams();
  const id = Number(noteId);
  const { data: fetchedNote } = useQuery({
    queryKey: ['note', { id }],
    queryFn: () => getNote(usernameFromURL as string, noteId as string),
    enabled: Boolean(note),
  });

  useEffect(() => {
    if (fetchedNote) {
      setValue(fetchedNote?.description);
    }
  }, [fetchedNote]);

  const [title, setTitle] = useState<string>(note?.title || '');
  const [value, setValue] = useState<string | undefined>(
    note ? note.description : template,
  );

  const { updateNote, createNote } = useNoteMenu();
  const { isUserPage } = useIsUserPage();
  const { isAuth, username } = useProfile();

  const [displayInput, setDisplayInput] = useState(false);

  const showTitleInput = !note || (note && displayInput && isUserPage);

  const [isOpenModal, setOpenModal] = useState(false);

  const [selectedIcon, setSelectedIcon] = useState<string | null>(note?.icon || null);

  const IconComponent = selectedIcon
    ? IconMap[selectedIcon as keyof typeof IconMap]
    : null;

  return (
    <>
      <div className={style.panel}>
        <Modal isOpen={isOpenModal} onClickClose={() => setOpenModal(false)}>
          <SelectIcon
            handleSelect={(iconName) => setSelectedIcon(iconName)}
            onClose={() => setOpenModal(false)}
          />
        </Modal>

        {IconComponent ? (
          <IconComponent onClick={() => setOpenModal(true)} />
        ) : (
          <Button onClick={() => setOpenModal(true)}>Выбрать иконку</Button>
        )}

        {!showTitleInput ? (
          <h2 onClick={() => setDisplayInput(true)} className={style.title}>
            {note.title}
          </h2>
        ) : (
          <input
            value={title}
            onChange={(e) => {
              setTitle(e.target.value);
            }}
            className={style.input}
            placeholder="Название заметки"
          ></input>
        )}
        {isAuth && isUserPage && (
          <div className={style.buttons}>
            {note && (
              <Button>
                <Link href={`/${username}`}>Новая заметка</Link>
              </Button>
            )}
            <Button
              onClick={() => {
                if (note) {
                  updateNote({
                    ...note,
                    title: title,
                    description: value,
                    icon: selectedIcon ? selectedIcon : undefined,
                    username: String(username),
                  });
                } else {
                  createNote({
                    title: title || 'Без названия',
                    description: value || '',
                    icon: selectedIcon ? selectedIcon : undefined,
                  });
                }
              }}
            >
              Сохранить
            </Button>
          </div>
        )}
      </div>

      <MDEditor
        preview={isUserPage ? undefined : 'preview'}
        className={style.editor}
        value={value}
        onChange={setValue}
      />
    </>
  );
};
