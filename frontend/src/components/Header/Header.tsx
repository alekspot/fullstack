'use client';

import { Dispatch, FC, SetStateAction, useEffect, useState } from 'react';
import styles from './Header.module.scss';
import { cn } from '@/helpers/classnames';
import { Search } from '../Search/Search';
import { Button } from '../Button/Button';
import { testReq } from '@/api/notes';
import { useProfile } from '@/providers/AuthProvider';

type Props = {
  isOpen: boolean;
  setOpenMenu: Dispatch<SetStateAction<boolean>>;
};

export const Header: FC<Props> = ({ isOpen, setOpenMenu }) => {
  return (
    <div className={styles.header}>
      <div
        className={cn(styles.placeholder, {
          [styles.isOpen]: isOpen,
        })}
      ></div>

      <Button onClick={() => setOpenMenu((isOpen) => !isOpen)}>Меню</Button>
      <Search />
      <a href="https://localhost:3006/api/backup/export">export</a>
    </div>
  );
};
