import styles from './MenuHeader.module.scss';
import Link from 'next/link';
import { useProfile } from '@/providers/AuthProvider';
import { FC } from 'react';
import { Button } from '@/components/Button/Button';
import { Logout } from '@/components/Auth/Logout/Logout';
import { LoginButton } from '@/components/Auth/LoginButton/LoginButton';

type Props = {
  handleClose: () => void;
};

export const MenuHeader: FC<Props> = ({ handleClose }) => {
  const profile = useProfile();

  return (
    <div className={styles.header}>
      <Button onClick={handleClose}>Закрыть</Button>
      {profile && <Link href={`/${profile.username}`}>{profile.username}</Link>}
      {profile.isAuth ? <Logout /> : <LoginButton />}
    </div>
  );
};
