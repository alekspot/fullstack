import styles from './ContextMenu.module.scss';
import { Button } from '@/components/Button/Button';
import { cn } from '@/helpers/classnames';
import { Note } from '@/types/Note';
import { useNoteMenu } from '@/hooks/useNoteMenu';
import { PushpinOutlined, DeleteOutlined } from '@ant-design/icons';
import { useIsUserPage, useProfile } from '@/providers/AuthProvider';

import { CSSTransition, TransitionStatus } from 'react-transition-group';
import { CSSProperties, Dispatch, SetStateAction, useCallback, useRef } from 'react';
import { useOutsideClick } from '@/hooks/useOutsideClick';

type Props = {
  note: Note;
  isOpen: boolean;
  handleClose: () => void;
};

const duration = 150;

// const transitionClass: Record<TransitionStatus, string> = {
//   entering: styles.entering,
//   entered: styles.entered,
//   exiting: styles.entering,
//   exited: styles.exited,
//   unmounted: '',
// };

const transitionClass = {
  enter: styles.entering,
  enterActive: styles.entered,
  exit: styles.exiting,
  exitActive: styles.exited,
};

export const ContextMenu = ({ note, isOpen, handleClose }: Props) => {
  const { isUserPage } = useIsUserPage();
  const { pinNote, deleteNote } = useNoteMenu();

  const handleClickPin = () => pinNote(note);
  const handleClickDelete = () => deleteNote(note.id);

  const nodeRef = useRef(null);

  useOutsideClick(nodeRef, handleClose);

  if (!isUserPage) {
    return null;
  }

  return (
    <CSSTransition
      nodeRef={nodeRef}
      in={isOpen}
      timeout={duration}
      mountOnEnter
      unmountOnExit
      classNames={transitionClass}
    >
      {(state) => (
        <div ref={nodeRef} className={cn(styles.menu)}>
          <Button className={styles.button} onClick={handleClickPin}>
            <PushpinOutlined />
            {note.pinned ? 'Открепить' : 'Закрепить'}
          </Button>
          <Button
            className={cn(styles.button, styles.danger)}
            onClick={handleClickDelete}
          >
            <DeleteOutlined />
            Удалить
          </Button>
        </div>
      )}
    </CSSTransition>
  );
};
