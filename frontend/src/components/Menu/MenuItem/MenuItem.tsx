'use client';

import {
  Dispatch,
  FC,
  MouseEventHandler,
  MutableRefObject,
  SetStateAction,
  useEffect,
  useLayoutEffect,
  useRef,
  useState,
} from 'react';
import styles from './MenuItem.module.scss';
import cn from 'classnames';
import { useOutsideClick } from '@/hooks/useOutsideClick';

import { Note } from '@/types/Note';
import { Button } from '@/components/Button/Button';
import { useNoteMenu } from '@/hooks/useNoteMenu';
import { useIsUserPage, useProfile } from '@/providers/AuthProvider';
import { useParams } from 'next/navigation';
import { PortalWithTransition } from '@/components/Portal/PortalWithTransition';
import { createPortal } from 'react-dom';
import { CSSTransition } from 'react-transition-group';

type Props = {
  isActive: boolean;
  isOpenMenu: boolean;
  children: React.ReactNode;
  onClick: () => void;
  onRightClick: () => void;
  renderMenu: React.ReactNode;
  renderRight?: React.ReactNode;
};

export const MenuItem: FC<Props> = ({
  isActive,
  isOpenMenu,
  children,
  onClick,
  onRightClick,
  renderMenu,
  renderRight = null,
}) => {
  const ref = useRef<HTMLDivElement | null>(null);

  const { isAuth, username } = useProfile();
  const { isUserPage } = useIsUserPage();

  const nodeRef = useRef(null);

  const getPosition = (ref: MutableRefObject<HTMLDivElement | null>) => {
    if (!ref.current) {
      return undefined;
    }

    const { right, top } = ref.current.getBoundingClientRect();

    return { '--x': `${right}px`, '--y': `${top}px` } as unknown as any;
  };

  const renderContextMenu =
    ref.current &&
    isUserPage &&
    username &&
    createPortal(
      <CSSTransition
        nodeRef={nodeRef}
        in={isOpenMenu}
        timeout={300}
        mountOnEnter
        unmountOnExit
        classNames={{
          enter: styles['wrap-enter'],
          enterActive: styles['wrap-enter-active'],
          exit: styles['wrap-exit'],
          exitActive: styles['wrap-exit-active'],
        }}
      >
        <div ref={nodeRef} style={getPosition(ref)} className={styles.wrap}>
          {renderMenu}
        </div>
      </CSSTransition>,
      document.body,
    );

  return (
    <div
      ref={ref}
      onClick={onClick}
      onContextMenu={(e) => {
        e.preventDefault();

        onRightClick();
      }}
      className={cn(styles.item, {
        [styles.active]: isActive,
      })}
    >
      <div className={styles.title}>{children}</div>
      {renderRight}
      {renderMenu}
    </div>
  );
};
