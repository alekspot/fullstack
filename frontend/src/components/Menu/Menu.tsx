'use client';
import { Dispatch, FC, SetStateAction, useEffect, useState } from 'react';
import styles from './Menu.module.scss';
import { cn } from '../../helpers/classnames';
import { useQuery } from '@tanstack/react-query';
import { getNotes } from '@/api/notes';
import { useParams, useRouter } from 'next/navigation';
import { Sidebar } from '../Sidebar/Sidebar';
import { MenuItem } from './MenuItem/MenuItem';
import { List } from '../List/List';
import { useMobile } from '@/hooks/useMobile';
import { MenuHeader } from './MenuHeader/MenuHeader';
import { SwipeList } from '../SwipeList/SwipeList';
import { useNoteMenu } from '@/hooks/useNoteMenu';
import { useProfile } from '@/providers/AuthProvider';
import { ContextMenu } from './ContextMenu/ContextMenu';
import { Icons } from './Icons/Icons';

type Props = {
  isOpen: boolean;
  setOpen: Dispatch<SetStateAction<boolean>>;
};

export const Menu: FC<Props> = ({ isOpen, setOpen }) => {
  const { username: usernameFromUrl } = useParams();
  const { data: notes = [] } = useQuery({
    queryKey: ['notes'],
    queryFn: () => getNotes(usernameFromUrl as string),
  });

  const pinnedNotes = notes.filter(({ pinned }) => pinned);
  const restNotes = notes.filter(({ pinned }) => !pinned);

  const sortedNotes = [...pinnedNotes, ...restNotes];

  const { username } = useProfile();

  useEffect(() => {
    if (notes.length > 0) {
      setOpen(true);
    }
  }, [notes]);

  const router = useRouter();

  const { noteId } = useParams();

  const [activeId, setActiveId] = useState(Number(noteId));
  const [hoverId, setHoverId] = useState<number | null>(null);

  const [openId, setOpenId] = useState<number | null>(null);

  const { isMobile } = useMobile();

  const ListComponent = isMobile ? SwipeList : List;

  const closeSidebar = () => setOpen(false);

  return (
    <Sidebar isOpen={isOpen}>
      <MenuHeader handleClose={closeSidebar} />
      <ListComponent
        groupItems={[pinnedNotes]}
        items={restNotes}
        renderItem={(note, index, isActive) => (
          <MenuItem
            isOpenMenu={note.id === openId}
            isActive={isActive}
            onClick={() => {
              if (isMobile) {
                closeSidebar();
              }
              setActiveId(note.id);
              setOpenId(null);
              router.push(`/${usernameFromUrl}/${note.id}`);
            }}
            onRightClick={() => {
              setOpenId(note.id);
            }}
            renderRight={<Icons note={note} />}
            renderMenu={
              <ContextMenu
                isOpen={note.id === openId}
                note={note}
                handleClose={() => setOpenId(null)}
              />
            }
          >
            {note.title}
          </MenuItem>
        )}
        getItemClassName={(note) => cn({ [styles.pinned]: note.pinned })}
        isActive={(note) => note.id === activeId}
      />
    </Sidebar>
  );
};
