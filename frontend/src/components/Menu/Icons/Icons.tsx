import { useState } from 'react';
import style from './Icons.module.scss';
import { PushpinOutlined, DeleteOutlined } from '@ant-design/icons';
import { Note } from '@/types/Note';
import { IconMap } from '@/components/SelectIcon/SelectIcon';

type Props = { note: Note };

export const Icons = ({ note }: Props) => {
  const IconComponent = note.icon ? IconMap[note.icon as keyof typeof IconMap] : null;

  return (
    <div className={style.icons}>
      {IconComponent && <IconComponent />}
      {note.pinned && <PushpinOutlined />}
    </div>
  );
};
