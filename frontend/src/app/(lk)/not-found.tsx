import { Logout } from '@/components/Auth/Logout/Logout';

export default function NotFound() {
  return (
    <div>
      <h2>Not Found</h2>
      <Logout />
    </div>
  );
}
