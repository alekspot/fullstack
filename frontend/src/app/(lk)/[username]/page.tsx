import { Editor } from '@/components/Editor/Editor';
import { notFound } from 'next/navigation';
import styles from './page.module.scss';

type Props = {
  params: {
    username: string;
  };
};

const BFF_URI = process.env.BFF_URI;

export default async function UserPage({ params: { username } }: Props) {
  const users: { username: string }[] = await fetch(`${BFF_URI}/api/users/all`, {
    cache: 'no-store',
  }).then((res) => res.json());

  if (!users.find((u) => u.username === username)) {
    notFound();
  }

  return (
    <main className={styles.main}>
      <Editor />
    </main>
  );
}
