import { Editor } from '@/components/Editor/Editor';
import type { Metadata } from 'next';
import styles from './page.module.scss';
import { notFound } from 'next/navigation';
import { Note } from '@/types/Note';

type Props = {
  params: {
    username: string;
    noteId: string;
  };
};

const BFF_URI = process.env.BFF_URI;

export async function generateMetadata({ params }: Props): Promise<Metadata> {
  const note = await fetch(
    `${BFF_URI}/api/notes/${params.username}/${params.noteId}`,
  ).then((res) => res.json());

  return {
    title: note.title,
    openGraph: {
      title: note.title,
      type: 'website',
      description: note.description,
      url: `https://2461348-xj88721.twc1.net/${params.username}/${params.noteId}`,
      images: ['https://all-aforizmy.ru/wp-content/uploads/2021/11/original_planeti.jpg'],
    },
  };
}

export default async function NotePage({ params: { username, noteId } }: Props) {
  const note: Note = await fetch(`${BFF_URI}/api/notes/${username}/${noteId}`, {
    cache: 'no-store',
  }).then((res) => {
    if (res.status === 404) {
      notFound();
    }
    return res.json();
  });

  if (!note) {
    notFound();
  }

  return (
    <main className={styles.main}>
      <Editor note={note} />
    </main>
  );
}
