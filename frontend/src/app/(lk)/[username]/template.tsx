'use client';

import { motion } from 'framer-motion';
import styles from './template.module.scss';

export default function Transition({ children }: { children: React.ReactNode }) {
  return (
    <motion.div
      transition={{ duration: 0.8 }}
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      className={styles.flex}
    >
      {children}
    </motion.div>
  );
}
