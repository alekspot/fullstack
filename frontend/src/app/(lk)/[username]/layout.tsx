'use client';
import { Header } from '@/components/Header/Header';
import { useState } from 'react';
import styles from './layout.module.scss';
import { cn } from '@/helpers/classnames';
import { Menu } from '@/components/Menu/Menu';

type Props = {
  params: {
    username: string;
    noteId: string;
  };
  children: React.ReactNode;
};

export default function Layout({ children }: Props) {
  const [isOpen, setOpen] = useState(false);

  return (
    <main className={styles.wrap}>
      <Header isOpen={isOpen} setOpenMenu={setOpen} />

      <Menu isOpen={isOpen} setOpen={setOpen} />

      <div className={styles.content}>
        <div
          className={cn(styles.placeholder, {
            [styles.isOpen]: isOpen,
          })}
        ></div>
        {children}
      </div>
    </main>
  );
}
