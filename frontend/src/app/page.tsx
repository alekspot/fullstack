'use client';

import { useProfile } from '@/providers/AuthProvider';
import styles from './page.module.css';
import { LoginButton } from '@/components/Auth/LoginButton/LoginButton';

export default function Home() {
  const { isAuth } = useProfile();

  if (isAuth === null || isAuth === true) {
    return (
      <main className={styles.main}>
        <h1>Загрузка...</h1>
      </main>
    );
  }

  return (
    <main className={styles.main}>
      {/* <h1>Привет, давай заходи...</h1> */}
      <LoginButton />
    </main>
  );
}
